
# gzipit  

Compress large files using your machine's full parallelism.

---

Let's start with the example of an `export`ed docker container roughly 1.1G in size; suppose you
want to SCP it over the internet, but not have to wait for the full 1 Gigabytes
to transfer.

The traditional command line approach takes maybe too long, however:

```
$ time gzip -c < friendly_newton.tar  > friendly_newton.tgz

real	0m33.978s
user	0m33.421s
sys	0m0.456s
```

But we're only using one processor.  Most recently manufactured machines, even laptops
not made for gaming, have at least four cores to work with.

This is where `gzipit`, essentially a Bash script that divides your gzip target
into smaller chunks and zips them independently, can give you a small time advantage.

`gzipit` also uses parallelism to check the zipped-up chunks against their respective parts in the original file, for a very speedy experience.

You need gzip itself -- in addition to both Perl and GNU parallel -- to be installed for  `gzipit` to do its thing:

```
$ ./gzipit -Kv friendly_newton.tar
parallel gzip starting
parallel gzip done.Duration: 8 seconds
testing chunks in parallel ...success!  chunks reconstructed to a complete match

$ ls -lart
		...

-rw-rw-r--  1 daniel daniel 1180100096 Apr  3 08:15 friendly_newton.tar
-rw-rw-r--  1 daniel daniel  401916560 Apr  3 08:19 friendly_newton.tgz
-rw-rw-r--  1 daniel daniel  401904686 Apr  3 08:22 friendly_newton.tar.gz
drwxrwxr-x  3 daniel daniel       4096 Apr  3 08:22 .
```

You can see it has produced a compressed image comparable in size to what gzip produces
with no extra optimization, and in a third the time. (This is on a machine with 8
hyperthreaded cores, though that probably translates to more like 4 in an application
such as this one.)

Compare that with just the time it takes to verify, via traditional shell commands, that 
the compressed image is a faithful copy of the original:
```
$ time { gzip -dc <friendly_newton.tar.gz | diff - friendly_newton.tar ; }

real	0m5.539s
user	0m6.167s
sys	0m1.684s
```

Or for those who prefer a less tricksy approach:

```
$ gzip -dc < friendly_newton.tar.gz > friendly_newton__RECONSTRUCTED.tar
$ diff friendly_newton__RECONSTRUCTED.tar friendly_newton.tar
$ echo $?  
0
```
