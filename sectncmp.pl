#!/usr/bin/perl
use Getopt::Std;
use File::stat;
use Compress::Zlib;
sub min { $_[0] < $_[1] ? $_[0] : $_[1] } 
our (%o);
getopts("f:",\%o);
our $buflen = 1024 ** 2;
#-- call as: $0 first_file.gz  second_file  seek_offs  length_in_bytes
our ( $g,) = gzopen($ARGV[0],"rb") or die "first file (.gz)";
our ($fh);
open ($fh,"<",$ARGV[1]) and binmode($fh) or die "second file";
$fh->seek($ARGV[2],0) if $ARGV[2];
our $len = $ARGV[3] || 0;
our $offs = 0;
our $rep = 0;
our $total = 0;
while ($offs < $len) {
    my ($buf1,$buf2);
    my $a = $g->gzread($buf1,min($len-$offs,$buflen));
    my $len = length($buf1);
    $total += $len;
    last if $len == 0;
    my $b = $fh->read($buf2,$len);
    die "different at +$offs\n" unless ($buf1 eq $buf2);
    $offs += $len;
}
print STDOUT ($o{f}," ",$total," ",$fh->tell(),"\n") if $o{f} ne "";
