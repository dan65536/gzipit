#!/bin/bash
if ! which parallel >/dev/null 2>&1; then
  echo >&2 "system lacks GNU 'parallel' (or the 'which' command)"; exit 1
fi
if ! which shasum >/dev/null 2>&1; then
  if which sha256sum >/dev/null 2>&1; then
    shasum() {
      [ ".$1" = ".-a" ] && [ "$2" = "256" ] || { exit 7; }
      shift 2
      sha256sum "$@"
    }
  else
    echo >&2 "WARNING - shasum or sha256sum needed for some functions"
  fi
fi
quit=""
check=""
verbose="0"
cleanup="1"
delete_orig=""
processes=$(( $(lscpu -p|sed '/^#/d'|wc -l) * 90 / 100 ))
print_Y=""
minchunk=10000000

set_maxchunk() {
    local nbits=${1:-29}
    [ "$nbits" -eq "$nbits" ] 2>/dev/null || return 1
    # -- set global variables --
    MAXCHUNK=$(((1<<nbits)-1))
    maxchunk=$MAXCHUNK
}

set_maxchunk  # (default values)

chunk=0
while getopts ":ydvV:hp:c:Kk:qn:x:X:b:" opt; do
  case $opt in
    X) set_maxchunk $OPTARG || { 
         echo >&2 "bad arg to -X: '$OPTARG'. Must be positive integer < 32"; exit 1
       } ;;
    b) 
       chunk=$OPTARG
       if [[ $chunk =~ x[0-9][0-9] ]]; then
           chunk=$((MAXCHUNK*${chunk:1:2}/100))
       elif [ $chunk = x ]; then
           chunk=$MAXCHUNK
       fi
    ;;
    d) delete_orig=1;;
    v) verbose=1;;
    V) verbose=$OPTARG;;
    x) maxchunk=$OPTARG;;
    n) minchunk=$OPTARG;;
    q) quit="1";;
    k) check=$OPTARG;;
    K) check=2;;
    c) cleanup="$OPTARG";;
    p) [ $OPTARG -gt 0 ] && processes=$OPTARG ;;
    y) print_Y=1;;
    h) { echo  "$0 [options] file_to_be_gzipped" ;
         echo  "options:"
         echo  " -h   : print this help"
         echo  " -b   : bytes per chunk"
         echo  " -x   : maximum chunk size"
         echo  " -n   : minimum chunk size"
         echo  " -k N : check against target; -k 2 is same as -K"
         echo  " -K   : parallel and thorough check against target"
         echo  " -d   : delete target (only after a successful '-k'/'-K' check)"
         echo  " -p N : set the parallelism to N processes (currently $processes)"
         echo  " -c L : cleanup level (L = 0 means no cleanup)"
         echo  " -v   : verbose"
         echo  " -V N : ultra verbose (N = integer verbosity)"
         echo  " -q   : quit after ultra verbose (-Vn, n>2) output"
       } >&2; exit 2;;
    \?) echo >&2 $OPTARG not recognized;exit 2;;
    :) echo >&2 $OPTARG needs arg; exit 2;;
  esac
done
shift $((OPTIND-1))

#--------------------------------------
#echo M $MAXCHUNK m $maxchunk
#exit 0
#--------------------------------------

if [ $maxchunk -gt $MAXCHUNK ]; then
  maxchunk=$MAXCHUNK
  echo >&2 "maxchunk too big; setting back to $MAXCHUNK"
fi

: ${check:=2}

if [ "$check" = 2 ] && ! perl -e1 -MCompress::Zlib >/dev/null 2>&1 ; then
    echo >&2 "ERROR - perl Compress::Zlib module needed for an efficient verification stage ..."
    echo >&2 " Please install it and re-attempt this run if you need to verify the gzipped product."
    exit 8
fi

if [ $# -lt 1 ]; then
  echo >&2 'need argument of filename for input'
  exit 3
fi
rm -f "${1}"-_-*.gz
siz=`stat -c%s "$1"` || { echo >&2 "cannot determine size of file: $1"; exit 4; }

if [ $chunk -eq 0 ]; then
    chunk=$((siz/processes+1))
fi

[ $chunk -gt $minchunk ] || chunk=$minchunk
[ $chunk -lt $maxchunk ] || chunk=$maxchunk
maxidx=$((siz/chunk))
procfmt="%0$(echo -n $maxidx|wc -c)d"
[ "$verbose" -gt 1 ] && {
  echo processes $processes
  echo minchunk  $minchunk
  echo maxchunk  $maxchunk
  echo maxidx    $maxidx
  echo chunk     $chunk
  printf "siz   %20d\n" $siz
  printf "rcsiz %20d\n" $((chunk * (maxidx+1)))
  echo '*********************************'
  echo check     "'$check'"
  echo cleanup   "'$cleanup'"
  echo procfmt   $procfmt
} >&2
[ -n "$quit" ] && { echo >&2 "quitting [-q]" ; exit 5; }

# -- spawn parallel jobs

[ $verbose -gt 0 ] && { echo >&2 "parallel gzip starting"; t0=$(date +%s); }

for ((x=0 ; x<=maxidx ; x++))
do
    [ $(( x * chunk )) -ge $siz ] && { ((--x)); break; }
    X=`printf $procfmt $x`
    echo "dd bs=$chunk count=1 skip=$x  < '$1' |gzip -c >'${1}-_-$X.gz'"
done | parallel >/dev/null 2>&1
if [ $? != 0 ]; then
    echo >&2 "chunking gzip failed. parallel returned: $?";
    [ "$cleanup" = 0 ] || rm -f "${1}"-_-*.gz
    exit 6
fi
[ $verbose -gt 0 ] && {
    echo -n "parallel gzip done."
    [ -n "$t0" ] && echo Duration: $(( `date +%s` - t0 )) "seconds" || echo ""
} >&2

# -- either (0) construct zipfile without checking
#           (1) calculate and compare checksums
#       or: (2) check all chunks are accurate

status=1 # fail by default

case $check in
0)
    cat "${1}-_-"*.gz > "${1}".gz
    status=$? ;;
1)
    [ $verbose -gt 0 ] && echo >&2 -n "check:"
    chk0=$(cat "${1}-_-"*.gz | tee "${1}".gz |\
           gzip -dc | shasum -a 256 | awk '{print $1}') ; [ $verbose -gt 0 ] && echo >&2 -ne "\n gunzipped -> $chk0"
    chk1=$(shasum -a 256 <"${1}" | awk '{print $1}')    ; [ $verbose -gt 0 ] && echo >&2 -e  "\n original  -> $chk1"
    [ "$chk0" = "$chk1" ] && {
        status=0
    } ;;

2|?*)
    [ $verbose -gt 0 ] && echo -n testing chunks in parallel ...
    Y=$(for ((x=0;;x++)) ; do
        X=`printf $procfmt $x`
        [ ! -f "${1}-_-$X.gz" ]  && break
        echo "'$(dirname $0)'/sectncmp.pl -f '$X' '${1}-_-$X.gz' '${1}' $((chunk*x)) $chunk"
        done | parallel 2>/dev/null |sort -n )
    status=$?
    if [ -n "$print_Y" ]; then
        echo >&2 Y=$'\nY: ----------------\n'"$Y"$'\n   ----------------\n'
    fi
    if [ $status != 0 ]; then
        echo >&2 "some chunk(s) failed"
        status=33
    else
        offs=0; total=0                 # no chunks failed ...
        for ((x=0;;x++)); do            # so check all chunks accounted for and match in size
            [ $status -eq 0 ] || break
            read a b c
            [ -z "$a" ] && break
            total=$c
            [ $a -eq $x ] && [ $((offs+=b)) -eq $c ] || status=11
        done <<<"$Y"
        [ $total -eq `stat -c%s "${1}"` ] || status=22  # check total orig file size against chunk total
        [ $status = 0 ] && { [ $verbose -gt 0 ] &&\
                             echo >&2 "success!  chunks reconstructed to a complete match"
                             cat "${1}-_-"*.gz > "${1}".gz ; }
    fi
    [ $verbose -gt 0 ] && echo
    ;;
esac

if [ -n "$delete_orig" -a $status = 0 ]
then
    echo "deleting target '${1}'" >&2
    rm -fr "${1}"
fi

# -- clean up (the default unless '-c 0' specified)

[ "$cleanup" = 0 ] || rm -f "${1}"-_-*.gz

# -- final error indication

if [ $status -ne 0 ]
then
    echo "gzip (or optional check) failed.\n" >&2
    exit $status
fi
