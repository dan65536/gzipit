#!/bin/bash

export CHK=$(gunzip -c irods-externals.tar-_-0.gz |wc -c)

time {
for x in {0..6}
do

  echo "./sectncmp.pl -f irods-externals.tar-_-$x.gz \
  ../irods-externals.tar \
  \$((CHK*$x)) \
  \$CHK ; echo $x $?"

done | parallel
}
